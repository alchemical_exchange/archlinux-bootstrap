#!/usr/bin/env bash

function install_desktop_packages() {
  arch-chroot /mnt /bin/bash \
<<EOF
pacman --noconfirm -S \
baobab \
eog \
epiphany \
evince \
file-roller \
gdm \
gedit \
gnome-backgrounds \
gnome-calculator \
gnome-calendar \
gnome-clocks \
gnome-color-manager \
gnome-contacts \
gnome-control-center \
gnome-disk-utility \
gnome-photos \
gnome-session \
gnome-settings-daemon \
gnome-shell \
gnome-shell-extensions \
gnome-system-monitor \
gnome-terminal \
gnome-themes-extra \
gnome-weather \
gvfs \
gvfs-afc \
mousetweaks \
nautilus \
sushi \
xdg-user-dirs-gtk \
mutter \
sushi \
qemu \
libvirt \
openssh \
signal-desktop \
libreoffice-fresh \
flatpak \
virt-manager \
libvirt \
qemu \
qemu-arch-extra \
qemu-block-gluster \
qemu-block-iscsi \
qemu-block-rbd \
ovmf \
zsh \
zsh-completions \
remmina \
libvncserver \
freerdp \
keepassxc \
docker \
gnucash \
evolution \
transmission-gtk \
gimp \
krita \
gnome-tweaks
EOF

  arch-chroot /mnt /bin/bash \
<<EOF
flatpak install flathub org.freedesktop.Platform/x86_64/19.08 -y;
flatpak install flathub com.spotify.Client -y;
flatpak install flathub com.valvesoftware.Steam -y;
flatpak install flathub com.discordapp.Discord -y;
flatpak install flathub ch.protonmail.protonmail-bridge -y;
EOF
}


function configure_desktop() {
  arch-chroot /mnt /bin/bash \
<<EOF
systemctl enable gdm.service
EOF
}


function install_desktop() {
  install_desktop_packages
  configure_desktop
}
