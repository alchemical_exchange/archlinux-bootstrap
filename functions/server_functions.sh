#!/usr/bin/env bash

function install_server_packages() {
  arch-chroot /mnt /bin/bash \
<<EOF
pacman --noconfirm -S networkmanager
pacman --noconfirm -S qemu
pacman --noconfirm -S libvirt
pacman --noconfirm -S nfs-utils
pacman --noconfirm -S openssh
pacman --noconfirm -S openbsd-netcat
EOF
}


function setup_nfs() {
    arch-chroot /mnt /bin/bash \
<<EOF
mkdir /srv/nfs
echo "/srv/nfs        *(rw,sync)" >> /etc/exports
systemctl enable nfs-server.service
iptables -A INPUT -p tcp -m tcp --dport 2049 -j ACCEPT
iptables-save > /etc/iptables/iptables.rules
EOF
}


function enable_services() {
    arch-chroot /mnt /bin/bash \
<<EOF
systemctl enable iptables.service
systemctl enable libvirtd.service
systemctl enable sshd.service
iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
iptables-save > /etc/iptables/iptables.rules
EOF
}


function install_server() {
  install_server_packages
  enable_services
  setup_nfs
}
