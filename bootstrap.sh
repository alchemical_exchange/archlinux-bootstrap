#!/usr/bin/env bash
source functions/base_functions.sh
source functions/server_functions.sh
source functions/desktop_functions.sh

hostname="test"
domain="alch.exch"
root_device=null
server=false
swap_size='2G'

function usage()
{
  echo
  echo >&2 "Usage: $0 -s || -w"
  echo
  echo >&2 "  -d    Domain"
  echo >&2 "  -h    Hostname"
  echo >&2 "  -m    Swap Size"
  echo >&2 "  -r    Root device"
  echo >&2 "  -s    Install in server mode"
  echo
}

root_device=null

while getopts ':d:h:m:p:r:s' opt; do
  case ${opt} in
    d)
      domain=$OPTARG
      ;;
    h)
      hostname=$OPTARG
      ;;
    m)
      swap_size=$OPTARG
      ;;
    r)
      root_device=$OPTARG
      ;;
    s)
      server=true
      ;;
    *)
      echo "Error: Invalid arguements"
      usage
      ;;
  esac
done

#install_base "${root_device}" "${swap_size}" "${hostname}" "${domain}"

if [[ ${server} = true ]]; then
  # TODO Finish basic server install
  echo "Server install not available."
  exit 1
  install_server
else
  echo "Installing workstation packages."
  install_desktop
fi

echo "Updating root password"
arch-chroot /mnt /bin/bash \
<<EOF
echo root:!@lch3my | chpasswd
EOF

echo "Creating user account"
arch-chroot /mnt /bin/bash \
<<EOF
useradd -m -G wheel -s /usr/bin/zsh jsieben
echo jsieben:Kn0wl3dge | chpasswd
EOF

echo "Unmounting block devices and rebooting."
#umount -R /mnt
#reboot
