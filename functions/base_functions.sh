#!/usr/bin/env bash
readonly TEST_URL="archlinux.org"
readonly EFI_VARS_DIR="/sys/firmware/efi/efivars"
readonly BOOT_PARTITION_SIZE="512M"
readonly DEFAULT_SWAP_SIZE="2G"


# Helper functions used throughout the installation.
function select_yes_no() {
  accept=false
  select input in "Yes" "No"; do
    case ${input} in
      Yes )
        accept=true
        break
        ;;
      No )
        accept=false
        break
        ;;
    esac
  done
}


function progress-bar() {
  local duration=${1}

  # shellcheck disable=SC2004
  for (( elapsed=1; elapsed<=$duration; elapsed++ )); do
    for ((done=0; done<$elapsed; done++)); do
      printf "▇";
    done;
    for ((remain=$elapsed; remain<$duration; remain++)); do
      printf " ";
    done;

    # shellcheck disable=SC2017
    printf "| %s%%" $(( (($elapsed)*100)/($duration)*100/100 ));
    sleep 1
    printf "\r";
  done
  printf "\r";
}


# Internet access is required to install Arch Linux.
# The following function tests if internet connectivity has been setup.
# Ends script execution in the event there is no network available.
function test_internet_connection() {
  echo "Test internet connectivity with test url: TEST_URL"

  if ping -q -w 1 -c 1 ${TEST_URL} > /dev/null; then
    echo "Online."
  else
    echo "Offline. Connect to the internet to continue."
    exit 1
  fi
}


# Tests for the presence of UEFI variables in the booted system.
# Offer the user the option to install in UEFI mode or BIOS boot mode.
function test_for_efi_vars() {
  efi_mode=false
  echo "Test if system is booted with EFI."

  if [[ -d ${EFI_VARS_DIR} && "$(ls -A ${EFI_VARS_DIR})" ]]; then
    echo "EFI vars exist."
    efi_mode=true
    #select_yes_no
    #if [[ ${accept} = true ]]; then
    #  efi_mode=true
    #else
    #  efi_mode=false
    #fi
  else
    echo "EFI vars do not exist. Exiting."
    exit 1
    select_yes_no
    if [[ ${accept} = true ]]; then
      efi_mode=false
    else
      exit 1
    fi
  fi
}


# Sync the timedatectl service with ntp.
# Wait for 30 seconds before checking to give the service a chance to update.
function sync_time_with_ntp() {
  echo "Use timedatectl to synchronize clock with ntp."

  timedatectl set-ntp true
  # Wait for timedatectl update to complete
  progress-bar 30
  if timedatectl | grep "NTP service: active" > /dev/null; then
    echo "Ntp active. System clock updated."
  else
    echo "Ntp failed to update clock."
    exit 1
  fi
}


# Partition the root disk and prepare. The boot partition is placed first and
# swap second. This is to avoid calculations when automatically creating
# partitions.
function partition_disk() {
  root_device=$1
  swap_size=$2
  efi_mode=$3

  if [[ ${root_device} = null ]]; then
    lsblk
    read -p -r  $'Enter block device to use as root install location.\n' \
      root_device
  fi

  # Format block device as GPT, GUID Partition Table
  sgdisk -Z "${root_device}"

  # Create partition, assign partition type, assign label
  if [[ ${efi_mode} = true ]]; then
    sgdisk -n 0:0:+${BOOT_PARTITION_SIZE} -t 0:ef00 -c 0:"boot" "${root_device}"
  else
    sgdisk -n 0:0:+${BOOT_PARTITION_SIZE} -t 0:ef02 -c 0:"boot" "${root_device}"
  fi
  # TODO optional swap partition
  # sgdisk -n 0:0:+"${swap_size}" -t 0:8200 -c 0:"swap" "${root_device}"
  sgdisk -n 0:0:0 -t 0:8300 -c 0:"/" "${root_device}"

  # List out partitions
  gdisk -l "${root_device}"
  #echo "Accept partitioning scheme?"
  #select_yes_no
  #if [[ ! ${accept} = true ]]; then
  #  # Clear partitions if user doesn't accept
  #  echo "Removing partitions."
  #  sgdisk -Z "${root_device}"
  #fi
}

# Create logical volume groups for the root device and swap
function setup_lvm() {
  physical_device=$1
  swap_size=$2
  hostname=$3
  lvm_partition=$(blkid | awk '{print substr($1, 0, length($1)-1)}' | grep "${physical_device}.*2")

  pvcreate "${lvm_partition}"
  vgcreate "${hostname}_vg" "${lvm_partition}"
  lvcreate -n "${hostname}_swap_lv" -L "${swap_size}" "${hostname}_vg"
  lvcreate -n "${hostname}_root_lv" -l 100%FREE "${hostname}_vg"
}


# Format the partitions. Currently only supports formating the root partition
# as btrfs. Btrfs supports dynamically adding additional disks which is useful
# for server installations.
function format_partitions() {
  root_device=$1
  hostname=$2
  boot_partition=$(blkid | awk '{print substr($1, 0, length($1)-1)}' | grep "${root_device}.*1")
  swap_volume="/dev/${hostname}_vg/${hostname}_swap_lv"
  root_volume="/dev/${hostname}_vg/${hostname}_root_lv"

  echo "Formating partitions."
  mkfs.fat -F32 "${boot_partition}"
  mkswap "${swap_volume}"
  mke2fs -t ext4 "${root_volume}"
}


# Mount partitions and turn on swap partition.
function mount_partitions() {
  root_device=$1
  hostname=$2
  boot_partition=$(blkid | awk '{print substr($1, 0, length($1)-1)}' | grep "${root_device}.*1")
  swap_volume="/dev/${hostname}_vg/${hostname}_swap_lv"
  root_volume="/dev/${hostname}_vg/${hostname}_root_lv"

  mount "${root_volume}" /mnt
  mkdir /mnt/boot
  mount "${boot_partition}" /mnt/boot
  swapon "${swap_volume}"
}


# Run the pacstrap command and install the base packages.
function run_pacstrap() {
  pacstrap /mnt base linux linux-firmware e2fsprogs exfat-utils lvm2 networkmanager vim sudo
}


# Setup initial required configuration. This includes generating the fstab file,
# generating the en_US.UTF-8 locale, setting the locale, and setting the
# hostname.
function initial_configuration() {
  hostname=$1
  domain=$2

  if [[ ${hostname} = null ]]; then
    read -p -r $'Enter the hostname for the computer.\n' \
      hostname
  fi
  if [[ ${domain} = null ]]; then
    read -p -r $'Enter the domain for the computer.\n' \
      domain
  fi

  # Generate fstab file
  genfstab -U /mnt >> /mnt/etc/fstab;
  cat /mnt/etc/fstab;

  # Basic system configuration
  arch-chroot /mnt /bin/bash \
<<EOF
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime;
hwclock --systohc;
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen;
locale-gen;
echo LANG=en_US.UTF-8 > /etc/locale.conf;
echo ${hostname} > /etc/hostname;
echo "127.0.0.1    localhost" >> /etc/hosts;
echo "::1          localhost" >> /etc/hosts;
echo "127.0.1.1    ${hostname}.${domain}    ${hostname}" >> /etc/hosts;
EOF

  # Edit sudoers file.
  arch-chroot /mnt /bin/bash \
<<EOF
echo 'foobar ALL=(ALL:ALL) ALL' | sudo EDITOR='tee -a' visudo
EOF

  # Enable network service.
  arch-chroot /mnt /bin/bash \
<<EOF
systemctl enable NetworkManager.service
EOF
}


function configure_mkinitcpio() {
  arch-chroot /mnt /bin/bash \
<<EOF
sed -i 's/MODULES=()/MODULES=(ext4)/g' /etc/mkinitcpio.conf;
sed -i 's/HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)/HOOKS=(base udev autodetect modconf block lvm2 resume filesystems keyboard fsck)/g' /etc/mkinitcpio.conf;
mkinitcpio -p linux;
EOF
}


# Install the bootloader and configure it. This includes adding the microcode
# package based on the detected cpu.
# TODO add support for BIOS boot with GRUB
function install_boot_loader() {
  physical_device=$1
  root_device=$(blkid | awk '{print substr($1, 0, length($1)-1)}' | grep "${physical_device}.*2")
  hostname=$2
  root_volume="/dev/${hostname}_vg/${hostname}_root_lv"
  efi_mode=$3
  if [[ ${efi_mode} = true ]]; then
    # Install systemd-boot
      arch-chroot /mnt /bin/bash \
<<EOF
    bootctl --path=/boot install;
EOF

    # Install microcode
    if [[ $(lscpu | grep "AuthenticAMD" | awk '{print $NF}') = "AuthenticAMD" ]]; then
      arch-chroot /mnt /bin/bash \
<<EOF
pacman --noconfirm -S amd-ucode;
EOF
      microcode=amd-ucode;
    elif [[ $(lscpu | grep "GenuineIntel" | awk '{print $NF}') = "GenuineIntel" ]]; then
      arch-chroot /mnt /bin/bash \
<<EOF
pacman --noconfirm -S intel-ucode;
EOF
      microcode=intel-ucode;
    fi

    # Package manager hook
    arch-chroot /mnt /bin/bash \
<<EOF
mkdir /etc/pacman.d/hooks
cat << HOOK > /etc/pacman.d/hooks/systemd-boot.hook;
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Updating systemd-boot
When = PostTransaction
Exec = /usr/bin/bootctl update
HOOK
EOF

    arch-chroot /mnt /bin/bash \
<<EOF
cat << CONF > /boot/loader/loader.conf
default       arch
timeout       0
console-mode  auto
editor        no
CONF

cat << ENTRY > /boot/loader/entries/arch.conf
title Arch Linux
linux /vmlinuz-linux
initrd /${microcode}.img
initrd /initramfs-linux.img
options root=${root_volume} rw
ENTRY
EOF
  else
    arch-chroot /mnt pacman -S grub;
  fi
}

# UUID=$(blkid -o value -s UUID "${root_volume}")

function install_base() {
  root_device=$1
  swap_size=$2
  hostname=$3
  domain=$4

  test_internet_connection
  test_for_efi_vars
  sync_time_with_ntp
  partition_disk "${root_device}" "${swap_size}" "${efi_mode}"
  setup_lvm "${root_device}" "${swap_size}" "${hostname}"
  format_partitions "${root_device}" "${hostname}"
  mount_partitions "${root_device}" "${hostname}"
  run_pacstrap
  initial_configuration "${hostname}" "${domain}"
  configure_mkinitcpio
  install_boot_loader "${root_device}" "${hostname}" "${efi_mode}"
}
